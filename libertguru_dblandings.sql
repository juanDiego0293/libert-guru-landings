-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 30-07-2018 a las 11:20:18
-- Versión del servidor: 10.0.35-MariaDB-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `libertguru_dblandings`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `designs`
--

CREATE TABLE `designs` (
  `iddesign` int(255) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` text,
  `user` int(225) DEFAULT NULL,
  `template` int(225) DEFAULT NULL,
  `edited` int(225) DEFAULT NULL,
  `created` int(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `designs`
--

INSERT INTO `designs` (`iddesign`, `status`, `name`, `code`, `user`, `template`, `edited`, `created`) VALUES
(22, 1, 'juan dev', '{\"global\":{\"logo\":\"http://libert-guru.com/tribu/landings/api/web/uploads/templates/2/logo.png\",\"logofooter\":\"http://libert-guru.com/tribu/landings/api/web/uploads/templates/2/logo-footer.png\",\"phonenumber\":\"311-999-9999\",\"redes\":{\"facebook\":\"#!\",\"twitter\":\"#!\",\"instagram\":\"#!\",\"youtube\":\"#!\",\"googleplus\":\"#!\"}},\"header\":{\"background\":\"http://libert-guru.com/tribu/landings/api/web/uploads/templates/2/254_01.jpg\",\"title\":{\"text\":\"Title is here\",\"color\":\"#FFF\",\"family\":\"\"},\"button\":{\"buttonText\":\"Button text is here\",\"linkButton\":\"#!\",\"background\":\"#f90\"}},\"section\":{\"left\":{\"imageleft\":\"http://libert-guru.com/tribu/landings/api/web/uploads/templates/2/img-left.jpg\",\"title\":{\"text\":\"Sunnyville Homes\"},\"description\":{\"value1\":{\"value\":\"2,5\",\"color\":\"#f90\"},\"value2\":{\"value\":\"baths\"}},\"footer\":{\"title\":\"Top features:\",\"content\":\"walk-in pantry, formal dining room, optional teen room, swimming pool.\"}},\"right\":{\"imageright\":\"http://libert-guru.com/tribu/landings/api/web/uploads/templates/2/img-right.jpg\",\"title\":{\"text\":\"Sunnyville Homes\"},\"description\":{\"value1\":{\"value\":\"2,5\",\"color\":\"#f90\"},\"value2\":{\"value\":\"baths\"}},\"footer\":{\"title\":\"Top features:\",\"content\":\"walk-in pantry, formal dining room, optional teen room, swimming pool.\"}}},\"section3\":{\"background\":\"http://libert-guru.com/tribu/landings/api/web/uploads/templates/2/pre-footer.jpg\",\"text\":\"Your dream home is just a click away.\",\"button\":{\"textbutton\":\"Text button\",\"linkbutton\":\"#!\",\"background\":\"#f90\"}}}', 2, 2, 1530382411, 1530382411);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `templates`
--

CREATE TABLE `templates` (
  `idtemplate` int(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` text,
  `edited` int(255) DEFAULT NULL,
  `created` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `templates`
--

INSERT INTO `templates` (`idtemplate`, `name`, `code`, `edited`, `created`) VALUES
(1, 'landing libert guru', '{}', NULL, NULL),
(2, 'Real estate', '{\"global\":{\"logo\":\"logo.png\",\"logofooter\":\"logo-footer.png\",\"phonenumber\":\"311-999-9999\",\"redes\":{\"facebook\":\"#!\",\"twitter\":\"#!\",\"instagram\":\"#!\",\"youtube\":\"#!\",\"googleplus\":\"#!\"}},\"header\":{\"background\":\"254_01.jpg\",\"title\":{\"text\":\"Title is here\",\"color\":\"#FFF\",\"family\":\"\"},\"button\":{\"buttonText\":\"Button text is here\",\"linkButton\":\"#!\",\"background\":\"#f90\"}},\"section\":{\"left\":{\"imageleft\":\"img-left.jpg\",\"title\":{\"text\":\"Sunnyville Homes\"},\"description\": {\"value1\": {\"value\":\"2,5\",\"color\":\"#f90\"},\"value2\":{\"value\":\"baths\"}},\"footer\":{\"title\":\"Top features:\",\"content\":\"walk-in pantry, formal dining room, optional teen room, swimming pool.\"}},\"right\":{\"imageright\":\"img-right.jpg\",\"title\":{\"text\":\"Sunnyville Homes\"},\"description\": {\"value1\": {\"value\":\"2,5\",\"color\":\"#f90\"},\"value2\":{\"value\":\"baths\"}},\"footer\":{\"title\":\"Top features:\",\"content\":\"walk-in pantry, formal dining room, optional teen room, swimming pool.\"}}},\"section3\":{\"background\":\"pre-footer.jpg\",\"text\":\"Your dream home is just a click away.\",\"button\":{\"textbutton\":\"Text button\",\"linkbutton\":\"#!\",\"background\":\"#f90\"}}}', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `iduser` int(255) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`iduser`, `status`, `name`, `nickname`, `email`, `created`) VALUES
(1, 1, 'Diego Vidal', 'deovidal', 'deovidal27@hotmail.com', 1529505487),
(2, 1, 'Juan Diego', 'juan', 'juan.diego.pinzon1993@gmail.com', 1529593439),
(10, 1, 'Michael', 'Millonario', 'guruauspiciador@libert-guru.com', 1530388395),
(11, 1, 'diego', 'diego', 'juan.15.d@hotmail.com', 1530388529);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `designs`
--
ALTER TABLE `designs`
  ADD PRIMARY KEY (`iddesign`);

--
-- Indices de la tabla `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`idtemplate`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `designs`
--
ALTER TABLE `designs`
  MODIFY `iddesign` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `templates`
--
ALTER TABLE `templates`
  MODIFY `idtemplate` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
