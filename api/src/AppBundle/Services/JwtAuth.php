<?php

namespace AppBundle\Services;

use Firebase\JWT\JWT;

class JwtAuth {
	public $manager;
	public $key;

	public function __construct($manager) {
		$this->manager = $manager;
		$this->key = "Clave-Secreta";
	}

	public function signup($email, $password, $getHash = NULL){
		$key = $this->key;

		$usuario = $this->manager->getRepository('BackBundle:Usuarios')->findOneBy(array(
			"email" => $email,	
			"password" => $password
		));

		if(is_object($usuario)){

			if($usuario->getEstado() == true){

	            // Se crea array del token con los datos necesarios de la DB
				$token = array(
					"idUsuario" => $usuario->getIdusuario(),
					"email" => $usuario->getEmail(),
					"idRol" => $usuario->getRol()->getIdrol(),
					"permisos" => $usuario->getRol()->getObj(),
					"obj" => $usuario->getObj()
				);

				$jwt = JWT::encode($token, $key, 'HS256');
				$decoded = JWT::decode($jwt, $key, array('HS256'));

				if($getHash != null){return $jwt;}else{return $decoded;}

			} else {
				return array("code" => 400,"msg" => "Este usuario está deshabilitado.");
			}	
		} else {
			return array("code" => 400,"msg" => "Usuario o contraseña incorrectos.");
		}
	}


	public function checkToken($jwt, $getIdentity = false){
		$key = $this->key;
		$auth = false;

		try{
			$decoded = JWT::decode($jwt, $key, array('HS256'));
		}catch(\UnexpectedValueException $e){
			$auth = false;
		}catch(\DomainExceptionException $e){
			$auth = false;
		}

		if(isset($decoded->idUsuario)){
			$auth = true;
		} else {
			$auth = false;
		}

		if($getIdentity == true){
			return $decoded;
		} else {
			return $auth;
		}
	}
}
