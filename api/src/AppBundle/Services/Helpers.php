<?php

namespace AppBundle\Services;

use BackBundle\Entity\Usuarios;
use \JFactory as JFactory;

class Helpers {

	public $jwt_auth;
	public $manager;

	public function __construct($jwt_auth, $manager) {
		$this->jwt_auth = $jwt_auth;
		$this->manager = $manager;
	}

	public function authCheck($hash, $getIdentity = false) {
		$jwt_auth = $this->jwt_auth;

		$auth = false;
		if ($hash != null) {
			if ($getIdentity == false) {
				$check_token = $jwt_auth->checkToken($hash);
				if ($check_token == true){$auth = true;}
			} else {
				$check_token = $jwt_auth->checkToken($hash, true);
				if (is_object($check_token)){$auth = $check_token;}
			}
		}
		return $auth;
	}

	public function json($data) {
		$normalizers = array(new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer());
		$encoders = array("json" => new \Symfony\Component\Serializer\Encoder\JsonEncoder());
		$serializer = new \Symfony\Component\Serializer\Serializer($normalizers, $encoders);
		$json = $serializer->serialize($data, 'json');
		$response = new \Symfony\Component\HttpFoundation\Response();
		$response->setContent($json);
		$response->headers->set("Content-Type", "application/json");
		return $response;
	}
	
	public function checkSession() {
		define( '_JEXEC', 1 );
		define( 'JPATH_BASE', realpath(dirname(__FILE__).'/../../../../../../tribu/administrator/' ));
		require_once ( JPATH_BASE. '/includes/defines.php' );
		require_once ( JPATH_BASE. '/includes/framework.php' );
		$mainframe = JFactory::getApplication('site');
		$mainframe->initialise();
		$user = JFactory::getUser();
		return $user;
	}

	public function checkUser($email) {
		$user = $this->manager->getRepository("BackBundle:Users")->findOneBy(array(
			"email" => $email
		));
		if (is_object($user)){
			return $user;
		} else {
			return false;	
		}
	}
}
