<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackBundle\Entity\Designs;

class DesignsController extends Controller {

	//LISTAR DISEÑOS
	public function listAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);	

			if(is_object($user)) {
				
				$designs = $em->getRepository('BackBundle:Designs')->findBy(array(
					"user" => $user->getIduser()
				));

				$data = array("code" => 200, "msg" => "Designs listings.", "data" => $designs);

			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}


	//OBTENER UN DISEÑO
	public function viewAction(Request $request, $idDesign) {
		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);	

			if(is_object($user)) {

				$design = $em->getRepository('BackBundle:Designs')->find($idDesign);

				if (is_object($design)) {
					$data = array("code" => 200, "msg" => "Design listed.", "data" => $design);
				} else {
					$data = array("code" => 400, "msg" => "There is no design with that ID");
				}
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}



	//UPLOAD IMAGE
	public function uploadAction(Request $request) {

		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);

			if(is_object($user)) {

				$json = $request->get("json", null);

				if ($json != null) {

					$params = json_decode($json);
					
					$idDesign = (isset($params->iddesign)) ? $params->iddesign : null;
					$image = (isset($params->image)) ? $params->image : null;
					
					if ($idDesign != null && $image != null) {

						$design = $em->getRepository('BackBundle:Designs')->findOneBy(array(
							"iddesign" => $idDesign
						));

						if (is_object($design)) {

							$ruta = "uploads/designs/";
							if (!is_dir($ruta)){mkdir($ruta, 0777);}

							$ruta .= $idDesign;
							if (!is_dir($ruta)){mkdir($ruta, 0777);}

							$baseFromJavascript = str_replace(' ', '+', $image);
							$base_to_php = explode(',', $baseFromJavascript);
							$data = base64_decode($base_to_php[1]); 
							$dateName = new \Datetime("now");
							$name = $dateName->format('Y_m_d_H_i_s');   
							$filepath = $ruta."/".$name.".png";
							file_put_contents($filepath, $data);

							$files = array_slice(scandir($ruta), 2);

							$data = array("code" => 200, "msg" => "Image uploaded", "name" => $name, "files" => $files);

						} else {
							$data = array("code" => 400, "msg" => "There is no design with that ID");
						}
					} else {
						$data = array("code" => 400, "msg" => "Incomplete params");
					}
				} else {
					$data = array("code" => 400, "msg" => "No params");
				}
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
		
	}


	//CREAR DISEÑOS
	public function createAction( Request $request) {
		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);

			if(is_object($user)) {

				$json = $request->get("json", null);

				if ($json != null) {

					$params = json_decode($json);
					$name = (isset($params->name)) ? $params->name : null;
					$code = (isset($params->code)) ? $params->code : null;
					$template = (isset($params->template)) ? $params->template : null;

					if ($name != null && $code != null && $template != null) {

						// Check Designs
						$haveDesign = $em->getRepository('BackBundle:Designs')->findOneBy(array(
							"user" => $user->getIduser()
						));

						$status = $haveDesign ? false : true;

						$createdDate = new \DateTime();
						$createdDate = $createdDate->getTimestamp();

						$design = new Designs();
						$design->setStatus($status);
						$design->setName($name);
						$design->setCode($code);
						$design->setUser($user->getIduser());
						$design->setTemplate($template);
						$design->setEdited($createdDate);
						$design->setCreated($createdDate);

						$em->persist($design);
						$em->flush();

						$routeTemplate = "uploads/templates/" . $template;

						if (is_dir($routeTemplate)){
							$files = array_slice(scandir($routeTemplate), 2);

							$routeDesign = "uploads/designs/";
							if (!is_dir($routeDesign)){mkdir($routeDesign, 0777);}

							$routeDesign .= $design->getIddesign();
							if (!is_dir($routeDesign)){mkdir($routeDesign, 0777);}

							foreach ($files as $file) {
								copy($routeTemplate."/".$file, $routeDesign."/".$file);
							}

							$data = array("code" => 200, "msg" => "Design created", "iddesign" => $design->getIddesign());
							
						} else {
							$data = array("code" => 400, "msg" => "The template files were not found");
						}

					} else {
						$data = array("code" => 400, "msg" => "Incomplete params");
					}
				} else {
					$data = array("code" => 400, "msg" => "No params");
				}
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}


	//EDITAR DISEÑOS
	public function editAction( Request $request) {
		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");
		$data = array("code" => 400,"msg" => "Error");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);

			if(is_object($user)) {

				$json = $request->get("json", null);

				if ($json != null) {

					$params = json_decode($json);
					$iddesign = (isset($params->iddesign)) ? $params->iddesign : null;
					$name = (isset($params->name)) ? $params->name : null;
					$code = (isset($params->code)) ? $params->code : null;

					if ($iddesign != null && $name != null && $code != null) {

						$design = $em->getRepository('BackBundle:Designs')->findOneBy(array(
							"iddesign" => $iddesign
						));

						if (is_object($design)) {

							$editedDate = new \DateTime();
							$editedDate = $editedDate->getTimestamp();

							$design->setName($name);
							$design->setCode($code);
							$design->setEdited($editedDate);

							$em->persist($design);
							$em->flush();

							$data = array("code" => 200, "msg" => "Design edited");
						} else {
							$data = array("code" => 400, "msg" => "There is no design with that ID");
						}
					} else {
						$data = array("code" => 400, "msg" => "Incomplete params");
					}
				} else {
					$data = array("code" => 400, "msg" => "No params");
				}
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}


	//ELIMINAR DISEÑOS
	public function deleteAction(Request $request, $idDesign) {
		$em = $this->getDoctrine()->getManager();
		$helpers = $this->get("app.helpers");
		$token = $request->get("token", null);
		$data = array("code" => 400,"msg" => "Error");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);

			if (is_object($user)) {

				$design = $em->getRepository("BackBundle:Designs")->findOneBy(array(
					"iddesign" => $idDesign
				));

				if(is_object($design)){

					$ruta = "uploads/designs/".$idDesign;
					if (file_exists($ruta)) {
						foreach(glob($ruta."/*.*") as $obj){unlink($obj);
						}
						rmdir($ruta);
					}

					$em->remove($design);
					$em->flush();

					$data = array("code" => 200, "msg" => "Design deleted.");

				} else {
					$data = array("code" => 400, "msg" => "There is no design with that ID");
				}
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}

}