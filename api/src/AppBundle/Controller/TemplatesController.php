<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackBundle\Entity\Templates;

class TemplatesController extends Controller {

	//LISTAR PLANTILLAS
	public function listAction( Request $request) {
		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);

			if(is_object($user)) {

				$templates = $em->getRepository('BackBundle:Templates')->findAll();

				$data = array("code" => 200, "msg" => "Templates listings.", "data" => $templates);
				
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}


	//OBTENER UNA PLANTILLA
	public function viewAction(Request $request, $idTemplate) {
		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);	

			if(is_object($user)) {
			
				$template = $em->getRepository('BackBundle:Templates')->find($idTemplate);

				if (is_object($template)) {
					$data = array("code" => 200, "msg" => "Template listed.", "data" => $template);
				} else {
					$data = array("code" => 400, "msg" => "There is no template with that ID");
				}
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}

}