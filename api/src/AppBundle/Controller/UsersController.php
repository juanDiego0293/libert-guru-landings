<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;
use BackBundle\Entity\Users;
use BackBundle\Entity\Designs;
use BackBundle\Entity\Templates;

class UsersController extends Controller {

	//CHECK USER
	public function checkAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);

			if(is_object($user)) {

				$designs = $em->getRepository('BackBundle:Designs')->findBy(array(
					"user" => $user->getIduser()
				));

				$templates = $em->getRepository('BackBundle:Templates')->findAll();

				$data = array("code" => 200, "msg" => "User exist", "designs" => $designs, "templates" => $templates);
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}


	//OBTENER DISEÑO ACTIVO
	public function activeAction(Request $request, $nickname) {
		$em = $this->getDoctrine()->getManager();
		$helpers = $this->get("app.helpers");

		$user = $em->getRepository('BackBundle:Users')->findOneBy(array(
			"nickname" => $nickname
		));

		$design = $em->getRepository('BackBundle:Designs')->findOneBy(array(
			"user" => $user->getIduser(),
			"status" => true
		));

		if ($design) {
			$data = array("code" => 200, "msg" => "design listed", "data" => $design);
		} else {
			$data = array("code" => 401, "msg" => "There is no active design for this user");
		}
		return $helpers->json($data);
	}


	//CREAR USUARIOS
	public function createAction( Request $request) {
		$em = $this->getDoctrine()->getManager();
		$helpers = $this->get("app.helpers");
		$json = $request->get("json", null);

		if ($json != null) {

			$params = json_decode($json);
			$name = (isset($params->name)) ? $params->name : null;
			$nickname = (isset($params->nickname)) ? $params->nickname : null;
			$email = (isset($params->email)) ? $params->email : null;

			if ($name != null && $nickname != null && $email != null) {

				$issetEmail = $em->getRepository('BackBundle:Users')->findOneBy(array(
					"email" => $email
				));

				if (!is_object($issetEmail)) {

					$issetNickname = $em->getRepository('BackBundle:Users')->findOneBy(array(
						"nickname" => $nickname
					));

					if (!is_object($issetNickname)) {

						$createdDate = new \DateTime();
						$createdDate = $createdDate->getTimestamp();

						$user = new Users();
						$user->setStatus(true);
						$user->setName($name);
						$user->setNickname($nickname);
						$user->setEmail($email);
						$user->setCreated($createdDate);

						$em->persist($user);
						$em->flush();

						$data = array("code" => 200, "msg" => "User created");

					} else {
						$data = array("code" => 400, "msg" => "Nickname is already in use");
					}
				} else {
					$data = array("code" => 400, "msg" => "Email is already in use");
				}
			} else {
				$data = array("code" => 400, "msg" => "Incomplete params");
			}
		} else {
			$data = array("code" => 400, "msg" => "No params");
		}
		return $helpers->json($data);
	}

}