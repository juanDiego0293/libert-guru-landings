<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller{

	public function indexAction(Request $request){
		return $this->render('default/index.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
		]);
	}

	//OBTENER ARCHIVOS
	public function getFilesAction( Request $request) {
		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);

			if(is_object($user)) {

				$json = $request->get("json", null);

				if ($json != null) {

					$params = json_decode($json);
					
					$route = (isset($params->route)) ? $params->route : null;
					
					if ($route != null) {

						if (is_dir($route)){
							$files = array_slice(scandir($route), 2);
							$data = array("code" => 200, "msg" => "Files listed", "data" => $files);
						} else {
							$data = array("code" => 400, "msg" => "Route does not exist");
						}
					} else {
						$data = array("code" => 400, "msg" => "Incomplete params");
					}
				} else {
					$data = array("code" => 400, "msg" => "No params");
				}
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}



	//BORRAR ARCHIVO
	public function deleteFilesAction( Request $request) {
		$em = $this->getDoctrine()->getManager();
		$token = $request->get("token", null);
		$helpers = $this->get("app.helpers");

		if ($token) {
			$session = $token;
		} else {
			$jml = $helpers->checkSession();
			$session = $jml->email;
		}

		if ($session) {

			$user = $helpers->checkUser($session);

			if(is_object($user)) {

				$json = $request->get("json", null);

				if ($json != null) {

					$params = json_decode($json);
					
					$route = (isset($params->route)) ? $params->route : null;
					
					if ($route != null) {


						if (file_exists($route)) {

							if (unlink($route)) {
								$files = array_slice(scandir(dirname($route)), 2);
								$data = array("code" => 200, "msg" => "File deleted", "data" => $files);
							} else {
								$data = array("code" => 400, "msg" => "Can not delete the file");
							}

						} else {
							$data = array("code" => 400, "msg" => "File does not exist");
						}
					} else {
						$data = array("code" => 400, "msg" => "Incomplete params");
					}
				} else {
					$data = array("code" => 400, "msg" => "No params");
				}
			} else {
				$data = array("code" => 400, "msg" => "User does not exist");
			}
		} else {
			$data = array("code" => 401, "msg" => "Invalid authorization");
		}
		return $helpers->json($data);
	}


	//TEST
	public function testAction(Request $request) {
		
		$data = array("data" => null);
		return $helpers->json($data);
		
	}
}
