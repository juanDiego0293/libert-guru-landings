<?php

namespace BackBundle\Entity;

/**
 * Designs
 */
class Designs
{
    /**
     * @var integer
     */
    private $iddesign;

    /**
     * @var boolean
     */
    private $status;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $user;

    /**
     * @var integer
     */
    private $template;

    /**
     * @var integer
     */
    private $edited;

    /**
     * @var integer
     */
    private $created;


    /**
     * Get iddesign
     *
     * @return integer
     */
    public function getIddesign()
    {
        return $this->iddesign;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Designs
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Designs
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Designs
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set user
     *
     * @param integer $user
     *
     * @return Designs
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set template
     *
     * @param integer $template
     *
     * @return Designs
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return integer
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set edited
     *
     * @param integer $edited
     *
     * @return Designs
     */
    public function setEdited($edited)
    {
        $this->edited = $edited;

        return $this;
    }

    /**
     * Get edited
     *
     * @return integer
     */
    public function getEdited()
    {
        return $this->edited;
    }

    /**
     * Set created
     *
     * @param integer $created
     *
     * @return Designs
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer
     */
    public function getCreated()
    {
        return $this->created;
    }
}

