<?php

namespace BackBundle\Entity;

/**
 * Templates
 */
class Templates
{
    /**
     * @var integer
     */
    private $idtemplate;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $edited;

    /**
     * @var integer
     */
    private $created;


    /**
     * Get idtemplate
     *
     * @return integer
     */
    public function getIdtemplate()
    {
        return $this->idtemplate;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Templates
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Templates
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set edited
     *
     * @param integer $edited
     *
     * @return Templates
     */
    public function setEdited($edited)
    {
        $this->edited = $edited;

        return $this;
    }

    /**
     * Get edited
     *
     * @return integer
     */
    public function getEdited()
    {
        return $this->edited;
    }

    /**
     * Set created
     *
     * @param integer $created
     *
     * @return Templates
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return integer
     */
    public function getCreated()
    {
        return $this->created;
    }
}

