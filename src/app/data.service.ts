import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,  HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
	headers: new HttpHeaders({
		'Content-Type':  'application/x-www-form-urlencoded'
	})
};

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private _http: HttpClient) { }

 	variables() {
		let vars = {
			urlBase: 'https://causalizadores.com/landings/api/web/app_dev.php/',
			designUploads: 'https://causalizadores.com/landings/api/web/uploads/designs/',
      templateUploads: 'https://causalizadores.com/landings/api/web/uploads/templates/',
      token: 'juan.diego.pinzon1993@gmail.com'
		};
		return vars
	};

	public url = this.variables().urlBase;

  checkUser(token) {
    let Params = new HttpParams();
    Params = Params.set('token', token);
    return this._http.post(this.url + 'users/check', Params, {headers: httpOptions.headers})
  }

  createUser(json) {
    let Params = new HttpParams();
    Params = Params.set('json', JSON.stringify(json));
    return this._http.post(this.url + 'users/create', Params, {headers: httpOptions.headers})
  }

  designActive() {
    let Params = new HttpParams();
    Params = Params.set('json', '');
    return this._http.post(this.url + 'users/active', Params, {headers: httpOptions.headers})
  }

  /*Plantillas*/

  getTemplates(token) {
  	let Params = new HttpParams();
    Params = Params.set('token', token);
    return this._http.post(this.url + 'templates/list', Params , { headers: httpOptions.headers })
  }

  viewTemplate(token, idTemplate) {
    let Params = new HttpParams();
    Params = Params.set('token', token);
    return this._http.post(this.url + 'templates/view/' + idTemplate, Params, {headers: httpOptions.headers})
  }

  /*Obtener Archivos*/
  getMedias(token, json) {
    let Params = new HttpParams();
    Params = Params.set('token', token).set('json', JSON.stringify(json));
    return this._http.post(this.url + 'getfiles', Params, {headers: httpOptions.headers})
  }

  /*Eliminar Archivos*/
  deleteMedia(token, json) {
    let Params = new HttpParams();
    Params = Params.set('token', token).set('json', JSON.stringify(json));
    return this._http.post(this.url + 'deletefiles', Params, {headers: httpOptions.headers})
  }

  /*Diseños*/

  /*Design in Production*/
  liveDesign(nickname) {
    let Params = new HttpParams();
    Params = Params.set('json', '');
    return this._http.post(this.url + 'users/active/' + nickname, Params, {headers: httpOptions.headers})
  }

  /*Subir imágenes*/
  uploadImages(token, json) {
    let Params = new HttpParams();
    Params = Params.set('json', JSON.stringify(json)).set('token', token);
    return this._http.post(this.url + 'designs/upload', Params, {headers: httpOptions.headers})
  }

  /*Listar Diseño por usuario*/ 
  getDesignByUser(token) {
    let Params = new HttpParams();
    Params = Params.set('token', token);
    return this._http.post(this.url + 'designs/list', Params, {headers: httpOptions.headers})
  }

  /*Crear Diseño*/
  createDesign(token,json) {
    console.log(JSON.stringify(json))
    let Params = new HttpParams();
    Params = Params.set('json', JSON.stringify(json)).set('token', token);
    return this._http.post(this.url + 'designs/create', Params, {headers: httpOptions.headers})
  }

  /*Editar Diseño*/
  editDesign(token, json) {
    let Params = new HttpParams();
    Params = Params.set('json', JSON.stringify(json)).set('token', token);
    return this._http.post(this.url + 'designs/edit', Params, {headers: httpOptions.headers})
  }

  /*Eliminar Diseño*/
  deleteDesign(idDesign, token) {
    let Params = new HttpParams();
    Params = Params.set('token', token);
    return this._http.post(this.url + 'designs/delete/' + idDesign, Params, {headers: httpOptions.headers})
  }

  /*Ver Diseño*/
  viewDesign(token,idDesign) {
    let Params = new HttpParams();
    Params = Params.set('token', token);
    return this._http.post(this.url + 'designs/view/' + idDesign, Params, {headers: httpOptions.headers})
  }
}
