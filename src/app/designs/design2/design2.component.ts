import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: 'app-design2',
  templateUrl: './design2.component.html',
  styleUrls: ['./design2.component.css']
})
export class Design2Component implements OnInit {
	@Input() design;
  constructor(
  	private sanitizer: DomSanitizer) { }

  ngOnInit() {
  	console.log(this.design);
  }
  transform(url) {
  	return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
