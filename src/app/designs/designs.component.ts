import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-designs',
  templateUrl: './designs.component.html',
  styleUrls: ['./designs.component.css']
})
export class DesignsComponent implements OnInit {
  public design:object = {};
  public alias:string;
  public numberTemplate:number;
  constructor(private _dataService: DataService,private _router: ActivatedRoute) { }

  ngOnInit() {
  	this._router.params.subscribe(params => {
      this.alias = params.alias;
    });

    this._dataService.liveDesign(this.alias).subscribe(response => {
  		console.log(response)
  		if (response['code'] == 200) {
  			this.numberTemplate = response['data'].template;
  			this.design = JSON.parse(response['data'].code);
  			console.log('design', this.design);
  		}else {
        console.log(response)
      }
  	});

  }

}
