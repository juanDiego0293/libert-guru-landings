import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../data.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-design1',
  templateUrl: './design1.component.html',
  styleUrls: ['./design1.component.css']
})
export class Design1Component implements OnInit {
	public token = this._dataService.variables().token;
	//public design = []
  public alias:string = '';
  @Input() design;
  constructor(private _dataService: DataService,
    private _router: ActivatedRoute) { }

  ngOnInit() {

   //  this._router.params.subscribe(params => {
   //    this.alias = params.alias;
   //  });

  	// this._dataService.liveDesign(this.alias).subscribe(response => {
  	// 	console.log(response)
  	// 	if (response['code'] == 200) {
  	// 		this.design = JSON.parse(response['data'].code);
  	// 		console.log('design', this.design);
  	// 	}else {
   //      console.log(response)
   //    }
  	// });

  }

}
