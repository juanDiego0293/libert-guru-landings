import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { ActivatedRoute, Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-landing2',
  templateUrl: './landing2.component.html',
  styleUrls: ['./landing2.component.css']
})
export class Landing2Component implements OnInit {
	public landing:object = {};
	public toggleActive:string = 'global';
	public confirmDesign:object = {open: false, message:'', msgDesign: ''};
	public medios:object = {open:false, message:''};
	public designName:string = '';
	public confirmDesignName:any;
	public nuevaImagen:any;
	public uploadImage:any;
	public token = this._dataService.variables();
	public urlImages:object = this._dataService.variables();
	public template = [];
	public medias = [];
	public idTemplate:number;
	public type:string;
	public updateImgDesign:any;
	public isTemplate: boolean = true;
	public isLoadTemplate: boolean = false;
	public nameDesign:string = ''
	public mediaSelected:number;
	public imageSelected:any;
	public mediaByMedios:string;
	public showButonSelected:boolean = false;

  constructor(
  	private _dataService: DataService,
	private _router: ActivatedRoute,
	private router: Router,
	private sanitizer: DomSanitizer) { }

  ngOnInit() {
  	this._router.params.subscribe(params => {
		console.log('params', params)
		this.idTemplate = params.id;
		this.type = params.tipo
	});

	/*this.landing = {
		global: {
			logo: 'logo.jpg',
			video: 'https://www.youtube.com/embed/1V1AqqQUiAY',
			button: {
				text: 'Quiero mi entrenamiento',
				background: '#f90',
				open: false
			}
		},
		header: {
			background: 'penthause.jpg',
			description: {
				text:'Quieres Tener Un Libro Mágico Y Las 3 Claves Que Te Permitan Crear Riqueza Y Conexión Espiritual Al Mismo Tiempo? Da clic en el Botón Y Dime A Que correo Te Lo Envío Totalmente Gratis!!! Si Deseas Tener Resultados Y Crear Abundancia, Usando Principios Universales... Debes Tener Este Entrenamiento',
				color: '#fff',
			}
		},
		form: {
			title: 'Descubre Como Crear Abundancia total',
			subtitle: 'Recibe Las 3 Claves Y Un Aplicativo Rad-Ionico Para Tu Celuar',
			footer: 'Recuerda Al Final Del Entrenamiento Reclamar Tus 4 Herramientas Rad-Iónicas, Para Tener Los Resultados Que Mereces… Toma Acción Cuanto Antes.'
		},
		section: {
			title: 'IMAGINA LO QUE VAS A RECIBIR',
			pasos: [
				{
					title: 'Día 1: Las 3 Variables Para crear Abundancia',
					icon: '',
					description: 'Recibirás un entrenamiento enfocado a que conozcas las 3 variables para crear libertad emocional, mental, espiritual y financiera…'
				},{
					title: 'Paso 2: Lo que te voy a Revelar Es Personal',
					icon: '',
					description: 'La herramienta y plataforma De Estudio, que te permitirá generar Abundancia en todas Las Áreas de Tu vida.'
				},{
					title: 'Paso 3: El Sistema',
					icon: '',
					description: 'El sistema educativo más poderoso para optimizar tu potencial de forma integral. **Pista, Gana El Juego De La Riqueza'
				},{
					title: 'Paso 4: Las Herramientas',
					icon: '',
					description: 'La herramienta que te permitirá crear ingresos por Internet sin página web, sin páginas de aterrizaje. Como por medio de las redes sociales puedes crear conexiones valiosas y monetizarlas.'
				},{
					title: 'Paso 5: Mas Allá De La Ley De Atraccion',
					icon: '',
					description: 'la herramienta con la que un Pasarás de la Ley De La atracción A La Ley De La CAUSALIZACION CONSCIENTE en muy poco tiempo y como tu la puedes usar.'
				},{
					title: 'Paso 6: Las Estrategias',
					icon: '',
					description: 'Este entrenamiento es TOTALMENTE GRATIS, aprenderás las estrategias que ya muchas personas estamos usando para que tu puedas llegar a Crear más De 1.500 U$D mensuales en muy corto tiempo;'
				},{
					title: 'Paso 7: El Sistema',
					icon: '',
					description: 'También veras nuestro sistema y como tu puedes literalmente copiarlo y usarlo para tu beneficio.'
				},{
					title: 'Paso 8: Honestidad',
					icon: '',
					description: 'No te cobraremos nada por el entrenamiento. Cuando conozcas nuestra plataforma  y el sistema que usamos; tu miras si quieres usarla… '
				},{
					title: 'Paso 9: Rad-Ionica Tecnología Mágica',
					icon: '',
					description: 'El poder de la Rad-iónica para crear abundancia por medio de activar tu campo electromagnético y así manifestar tu naturaleza abundante.'
				}
			]
		},
		prefooter: {
			background: '#ccc',
			imagen: 'extasis.gif',
			text: '¿Habías solicitado al universo respuestas?? Si en verdad has buscado e intentado ir mas allá de la ley de atracción, el secreto y en realidad cambiar tu vida de manera eficiente y crear riqueza de forma espiritual; esta Tecnología Rad-Ionica es para ti... Hoy Gratis'
		}
	}*/

	if (this.type == 'template') {

		this.template = [];
		this._dataService.viewTemplate(this.token.token, this.idTemplate).subscribe(response => {
			if (response['code'] == 200) {
				console.log(response)
				this.template = JSON.parse(response['data'].code);
				this.isLoadTemplate = true;
				this.template['global'].logo = this.urlImages['templateUploads'] + this.idTemplate + '/' + this.template['global'].logo;
				this.template['header'].background = this.urlImages['templateUploads'] + this.idTemplate + '/' + this.template['header'].background;
				this.template['prefooter'].imagen = this.urlImages['templateUploads'] + this.idTemplate + '/' + this.template['prefooter'].imagen;
				console.log("ghhhh",this.template)
			}
		})
	}else if (this.type == 'design' ) {
		this._dataService.viewDesign(this.token.token, this.idTemplate ).subscribe(response => {
			console.log(response);
			if (response['code'] == 200) {
				this.isTemplate = false;
				this.nameDesign = response['data'].name;
				console.log(response)
				this.template = JSON.parse(response['data'].code);
				console.log(this.template)
			}
		})
	}
  }

  	transform(url) {
    	return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  	}

  	public deleteMedia(media) {
		console.log(media);
		media = media.replace('https://causalizadores.com/landings/api/web/', '')
		const objDeleteMedia = {
			route: media
		}

		this._dataService.deleteMedia(this.token.token, objDeleteMedia).subscribe(response => {
			console.log('response', response)
			if (response['code'] == 200) {
				this.medias = []
				for(let media of response['data']) {
					media = this.urlImages['designUploads'] + this.idTemplate + '/' + media;
					this.medias.push(media);
				}
			}
		})
	}
	public gallery(img, editImgTemplate) {
		this.medias = [];
		console.log(editImgTemplate)
		console.log(img)
		const objRoute = {
			route: 'uploads/designs/' + this.idTemplate
		}
		console.log(objRoute)
		this._dataService.getMedias(this.token.token, objRoute).subscribe(response => {
			console.log(response)
			if (response['code'] == 200) {
				for(let media of response['data']) {
					media = this.urlImages['designUploads'] + this.idTemplate + '/' + media;
					this.medias.push(media);
				}
				this.medios['open'] = true;
			}
		});

		this.nuevaImagen = function ($event) {
			this.uploadImage($event.target);
		}

		this.uploadImage = function (obj) {
			var file:File = obj.files[0];
			var myReader:FileReader = new FileReader();

			myReader.onloadend = (e) => {
				let objNewImg = {
					iddesign: this.idTemplate,
					image: myReader.result
				}
				this.uploadImg(objNewImg);
			}
			myReader.readAsDataURL(file);
		}

		this.imageSelected = function (indexImage, media) {
			this.mediaSelected = indexImage;
			this.mediaByMedios = media;
			this.showButonSelected = true;
		}

		this.updateImgDesign = function (media) {
			console.log('media', media);

			switch (editImgTemplate) {
				case "logo":
					this.template.global.logo = media;
					this.medios['open'] = false;
					break;

				case "logo footer":
					this.template.global.logofooter = media;
					this.medios['open'] = false;
					break;

				case "header background":
					this.template.header.background = media;
					this.medios['open'] = false;
					break;

				case "product1":
					this.template.section.left.imageleft = media;
					this.medios['open'] = false;
					break;

				case "product2":
					this.template.section.right.imageright = media;
					this.medios['open'] = false;
					break;

				case "background pre footer":
					this.template.section3.background = media;
					this.medios['open'] = false;
					break;

				default:
					this.medios['open'] = false;
					break;
			}

			this.mediaSelected = null;
		}
	}
	public uploadImg(obj){
		this._dataService.uploadImages(this.token.token, obj).subscribe(response => {
			if (response['code'] == 200) {
				this.medias = [];
				console.log(response)
				for(let media of response['files']) {
					media = this.urlImages['designUploads'] + this.idTemplate + '/' + media;
					this.medias.push(media);
				}
			}
		});
	}

  	public saveLanding(obj) {
		console.log(obj);

		this.confirmDesign = {
			open: true,
			message: 'Vamos a darle un nombre a tu diseño'
		}

		this.confirmDesignName = function (status) {
			if (status) {
				const objDesign = {
					name: this.designName,
					code: JSON.stringify(obj),
					template: this.idTemplate
				}
				this._dataService.createDesign(this.token.token, objDesign).subscribe(response => {
					if (response['code'] == 200) {
						console.log('diseño creado');
						this.confirmDesign = {
							open: false,
							message: ''
						}
						this.isTemplate = false;
						this.isLoadTemplate = true;
						this.router.navigate(['/landings/design/libert-guru/' + response['iddesign']]);
					}
				})
			}
		} 	
	}
	public uploadLandign(obj) {
		console.log(obj);

		const objUpdate = {
			iddesign: this.idTemplate,
			name: this.nameDesign || this.designName,
			code: JSON.stringify(obj)
		}

		this._dataService.editDesign(this.token.token, objUpdate).subscribe(response => {
			console.log('response', response);
			if (response['code'] == 200) {
				console.log(response)
				this._dataService.getDesignByUser(this.token.token).subscribe(designs => {
					console.log(designs);
					if (response['code'] == 200) {
						for(let design of designs['data']) {
							if (design.iddesign == this.idTemplate && design.status) {
								this.confirmDesign = {
									open: true,
									message: 'Tu landing en linea',
									msgDesign: 'Hemos publicado tu landing recuerda que pueder verla en http://libert-guru.com/landings/design/tu-alias'
								}
							}else {
								this.confirmDesign = {
									open: true,
									message: 'nuevo diseño',
									msgDesign: 'ya tienes un diseño activo puedes verlo en http://libert-guru.com/landings/design/tu-alias'
								}
							}
						}
					}
				})
			}
		})
	}

}
