import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
	public landing:object;
	public background: string;
  public design:boolean = true;
  public nameTemplate:number;
  public template:string;
  public type:string = '';
  public idTemplate:number;

  constructor(private _dataService: DataService,
    private _router: ActivatedRoute,
    private router: Router) {  }

  ngOnInit() {
    this.idTemplate = 0;

    console.log('this._router', this._router)

    this._router.params.subscribe(params => {
      console.log('params', params);
      this.type = params.tipo;
      this.template = params.template;
    });

    //this.template = this.params.id
    console.log('template', this.template)


    //this.router.navigate(['landing/', this.type, this.template, this.template]);
  }
}
