import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
	selector: 'app-landing1',
	templateUrl: './landing1.component.html',
	styleUrls: ['./landing1.component.css']
})
export class Landing1Component implements OnInit {
	public landing:object = {};
	public toggleActive:string = 'global';
	public confirmDesign:object = {open: false, message:'', msgDesign: ''};
	public medios:object = {open:false, message:''};
	public designName:string = '';
	public confirmDesignName:any;
	public nuevaImagen:any;
	public uploadImage:any;
	public token = this._dataService.variables();
	public urlImages:object = this._dataService.variables();
	public template = [];
	public medias = [];
	public idTemplate:number;
	public type:string;
	public updateImgDesign:any;
	public isTemplate: boolean = true;
	public isLoadTemplate: boolean = false;
	public nameDesign:string = ''
	public mediaSelected:number;
	public imageSelected:any;
	public mediaByMedios:string;
	public showButonSelected:boolean = false;

	constructor(private _dataService: DataService,
		private _router: ActivatedRoute,
		private router: Router) { }

	ngOnInit() {

		this._router.params.subscribe(params => {
			console.log('params', params)
			this.idTemplate = params.id;
			this.type = params.tipo
		});

		if (this.type == 'template') {
			this.template = [];
			this._dataService.viewTemplate(this.token.token, 2).subscribe(response => {
				if (response['code'] == 200) {
					console.log(response)
					this.template = JSON.parse(response['data'].code);
					this.template['global'].logofooter = this.urlImages['templateUploads'] + 2 + '/' + this.template['global'].logofooter;
					this.template['global'].logo = this.urlImages['templateUploads'] + 2 + '/' + this.template['global'].logo;
					this.template['header'].background = this.urlImages['templateUploads'] + 2 + '/' + this.template['header'].background;
					this.template['section'].left.imageleft = this.urlImages['templateUploads'] + 2 + '/' + this.template['section'].left.imageleft;
					this.template['section'].right.imageright = this.urlImages['templateUploads'] + 2 + '/' + this.template['section'].right.imageright;
					this.template['section3'].background = this.urlImages['templateUploads'] + 2 + '/' + this.template['section3'].background;
					console.log(this.template)
				}
			})
		}else if (this.type == 'design' ) {
			this._dataService.viewDesign(this.token.token, this.idTemplate ).subscribe(response => {
				console.log(response);
				if (response['code'] == 200) {
					this.isTemplate = false;
					this.isLoadTemplate = true;
					this.nameDesign = response['data'].name;
					console.log(response)
					this.template = JSON.parse(response['data'].code);
					console.log(this.template)
				}
			})
		}
	}// End OnInit

	public gallery(img, editImgTemplate) {
		this.medias = [];
		console.log(editImgTemplate)
		console.log(img)
		const objRoute = {
			route: 'uploads/designs/' + this.idTemplate
		}
		console.log(objRoute)
		this._dataService.getMedias(this.token.token, objRoute).subscribe(response => {
			console.log(response)
			if (response['code'] == 200) {
				for(let media of response['data']) {
					media = this.urlImages['designUploads'] + this.idTemplate + '/' + media;
					this.medias.push(media);
				}
				this.medios['open'] = true;
			}
		});

		this.nuevaImagen = function ($event) {
			this.uploadImage($event.target);
		}

		this.uploadImage = function (obj) {
			var file:File = obj.files[0];
			var myReader:FileReader = new FileReader();

			myReader.onloadend = (e) => {
				let objNewImg = {
					iddesign: this.idTemplate,
					image: myReader.result
				}
				this.uploadImg(objNewImg);
			}
			myReader.readAsDataURL(file);
		}

		this.imageSelected = function (indexImage, media) {
			this.mediaSelected = indexImage;
			this.mediaByMedios = media;
			this.showButonSelected = true;
		}

		this.updateImgDesign = function (media) {
			console.log('media', media);

			switch (editImgTemplate) {
				case "logo":
					this.template.global.logo = media;
					this.medios['open'] = false;
					break;

				case "logo footer":
					this.template.global.logofooter = media;
					this.medios['open'] = false;
					break;

				case "header background":
					this.template.header.background = media;
					this.medios['open'] = false;
					break;

				case "product1":
					this.template.section.left.imageleft = media;
					this.medios['open'] = false;
					break;

				case "product2":
					this.template.section.right.imageright = media;
					this.medios['open'] = false;
					break;

				case "background pre footer":
					this.template.section3.background = media;
					this.medios['open'] = false;
					break;

				default:
					this.medios['open'] = false;
					break;
			}

			this.mediaSelected = null;
		}
	}

	public deleteMedia(media) {
		console.log(media);
		media = media.replace('https://causalizadores.com/landings/api/web/', '')
		const objDeleteMedia = {
			route: media
		}

		this._dataService.deleteMedia(this.token.token, objDeleteMedia).subscribe(response => {
			console.log('response', response)
			if (response['code'] == 200) {
				this.medias = []
				for(let media of response['data']) {
					media = this.urlImages['designUploads'] + this.idTemplate + '/' + media;
					this.medias.push(media);
				}
			}
		})
	}

	public uploadImg(obj){
		this._dataService.uploadImages(this.token.token, obj).subscribe(response => {
			if (response['code'] == 200) {
				this.medias = [];
				console.log(response)
				for(let media of response['files']) {
					media = this.urlImages['designUploads'] + this.idTemplate + '/' + media;
					this.medias.push(media);
				}
			}
		});
	}

	public saveLanding(obj) {
		console.log(obj);

		this.confirmDesign = {
			open: true,
			message: 'Vamos a darle un nombre a tu diseño'
		}

		this.confirmDesignName = function (status) {
			if (status) {
				const objDesign = {
					name: this.designName,
					code: JSON.stringify(obj),
					template: 2
				}
				this._dataService.createDesign(this.token.token, objDesign).subscribe(response => {
					if (response['code'] == 200) {
						console.log('diseño creado');
						this.confirmDesign = {
							open: false,
							message: ''
						}
						this.isTemplate = false;
						this.isLoadTemplate = true;
						this.router.navigate(['/landings/design/real-state/' + response['iddesign']]);
					}
				})
				console.log("msgdgffgdfg",objDesign)
			}
		} 	
	}

	public uploadLandign(obj) {
		console.log(obj);

		const objUpdate = {
			iddesign: this.idTemplate,
			name: this.nameDesign || this.designName,
			code: JSON.stringify(obj)
		}

		this._dataService.editDesign(this.token.token, objUpdate).subscribe(response => {
			console.log('response', response);
			if (response['code'] == 200) {
				console.log(response)
				this._dataService.getDesignByUser(this.token.token).subscribe(designs => {
					console.log(designs);
					if (response['code'] == 200) {
						for(let design of designs['data']) {
							if (design.iddesign == this.idTemplate && design.status) {
								this.confirmDesign = {
									open: true,
									message: 'Tu landing en linea',
									msgDesign: 'Hemos publicado tu landing recuerda que pueder verla en http://libert-guru.com/landings/design/tu-alias'
								}
							}else {
								this.confirmDesign = {
									open: true,
									message: 'nuevo diseño',
									msgDesign: 'ya tienes un diseño activo puedes verlo en http://libert-guru.com/landings/design/tu-alias'
								}
							}
						}
					}
				})
			}
		})
	}
}
