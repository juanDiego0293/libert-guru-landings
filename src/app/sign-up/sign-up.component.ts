import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
	@Input() userJomla;
	public obj:object = {};

  constructor(private _dataService: DataService, private _routeActive: ActivatedRoute, private  _router: Router) { }

  ngOnInit() {
  	
  	this.obj = {
  		email: this.userJomla,
  		nickname:'',
      name: ''
  	};
  	
  	console.log(this.userJomla)
  }

  public signUp(obj) {
    console.log(obj)

    this._dataService.createUser(obj).subscribe(response => {
      if (response['code'] == 200) {
        console.log(response)
        this._router.navigate(['home/']);
        window.location.href = 'http://libert-guru.com/tribu/landings/home';
      }else {
        console.log(response)
      }
    });
  }

}
