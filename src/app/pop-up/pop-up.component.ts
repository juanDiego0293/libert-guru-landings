import { Component, OnInit, Injectable, Input } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.css']
})
export class PopUpComponent implements OnInit {
	@Input() statusPopUp;
  constructor() { }

  ngOnInit() {

  }

  public closePoUp() {
  	this.statusPopUp.open = false;
  }

}
