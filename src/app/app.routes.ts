import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from "@angular/core";

/*Componentes*/
import { HomeComponent } from './home/home.component';
import { LandingComponent } from './landing/landing.component';
import { Landing1Component } from './landing/landing1/landing1.component';
import { Landing2Component } from './landing/landing2/landing2.component';
import { Design1Component } from './designs/design1/design1.component';
import { Design2Component } from './designs/design2/design2.component';
import { DesignsComponent } from './designs/designs.component';

const appRoutes: Routes = [
	{path: '', redirectTo: '/home', pathMatch: 'full'},
	{path: 'home', component: HomeComponent},
	{path: 'landings/:tipo/:template/:id', component: LandingComponent},
/*	{path: 'landing/:tipo/real-state/:id', component: Landing1Component},
	{path: 'landing/:tipo/libert-guru/:id', component: Landing2Component},*/
	{path: 'design/:alias', component: DesignsComponent},
];

export const routes:ModuleWithProviders = RouterModule.forRoot(appRoutes);