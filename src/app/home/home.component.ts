import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

import { PopUpComponent } from '../pop-up/pop-up.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public landings = [];
  public user:object;
  public popUp:object = {};
  public designs = [];
  public token = this._dataService.variables();
  public confirm:object = {open:false, message:''};
  public confirmButton:any;

  constructor(private _dataService: DataService,
	private _popUps: PopUpComponent) { }

  ngOnInit() {

  	this._dataService.checkUser(this.token.token).subscribe(response => {
  		if (response['code'] == 401) {
  			this.popUp['open'] = true;
        this.popUp['title'] = 'No haz iniciado sesion en libert guru';
        this.popUp['redirectToLogin'] = true;
        console.log(response)
  		}else if (response['code'] == 200 ){
        this.designs = response['designs'];

        for(let design of this.designs) {
          console.log('design', design)

          if (design.template == 2) {
            design['alias'] = 'real-state';
          }else if (design.template == 1){
            design['alias'] = 'libert-guru';
          }
        }
        console.log('this.designs', this.designs)
        console.log(response);
  		}else {
        this.popUp['open'] = true;
        this.popUp['title'] = 'Registro';
        this.popUp['email'] = 'guruauspiciador@libert-guru.com';
        this.popUp['redirectToLogin'] = false;
        console.log(response)
      }
  	});

	this.landings = [{
		id: 1,
		alias: 'libert-guru',
		screenshot: 'screenshot-2.png',
		name: 'Landing Libert guru',
	},{
		id: 2,
		alias: 'real-state',
		screenshot: 'screenshot.png',
		name: 'Real estate',
	}]

	this._dataService.getTemplates(this.token.token).subscribe(response => {
		if (response['code'] == 200) {
		  	console.log(response);
		}
	});
  }//End OnInit

  deleteDesign(obj) {
  	this.confirm = {
  		open:true,
  		message: '¿Desea eliminar este diseño?'
  	}

  	this.confirmButton = function (status) {
  		if (status) {
  			this._dataService.deleteDesign(obj.iddesign, this.token.token).subscribe(response => {
  				if (response['code'] == 200) {
  					this.confirm = {open:false, message:''};
  					this._dataService.getDesignByUser(this.token.token).subscribe(designs => {
  						console.log(designs);
              this.designs = designs['data'];
  					});
  				}
  			})
  		}else {
  			this.confirm = {open:false, message:''};
  		}
  	}
  }
}
