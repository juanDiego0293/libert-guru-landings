import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routes } from './app.routes';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';

/*Servicios*/
import { DataService } from './data.service';

/*Color Picker*/
import { ColorPickerModule } from 'ngx-color-picker';

import { AppComponent } from './app.component';
import { TopHeaderComponent } from './top-header.component';
import { HomeComponent } from './home/home.component';
import { LandingComponent } from './landing/landing.component';
import { PopUpComponent } from './pop-up/pop-up.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { Landing1Component } from './landing/landing1/landing1.component';
import { Landing2Component } from './landing/landing2/landing2.component';
import { DesignsComponent } from './designs/designs.component';
import { Design1Component } from './designs/design1/design1.component';
import { Design2Component } from './designs/design2/design2.component';

@NgModule({
  declarations: [
    AppComponent,
    TopHeaderComponent,
    HomeComponent,
    LandingComponent,
    PopUpComponent,
    SignUpComponent,
    Landing1Component,
    Landing2Component,
    DesignsComponent,
    Design1Component,
    Design2Component
  ],
  imports: [
    FormsModule,
    BrowserModule,
    ColorPickerModule,
    HttpClientModule,
    routes
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
